<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('items','ItemsController@index');
Route::get('items/{item}','ItemsController@show');
Route::put('item/{item}','ItemsController@update');
Route::delete('items/{item}','ItemsController@destroy');



Route::get('categories',function(){
    $categories = \App\Category::withCount('items')->get();
    return $categories;
});