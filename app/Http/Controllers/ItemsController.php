<?php

namespace App\Http\Controllers;

use App\Item;
use Illuminate\Http\Request;

class ItemsController extends Controller
{
    //
    public function index() {
        $items = Item::with('categories')->get();
        return [
            'items'=>$items
        ];
    }

    public function show($item) {
//        $categories = $item->categories;
//        $item->categories=[];
//        $categories=[];

        $item = Item::with('categories')->find($item);
        return $item;
    }

    public function destroy(Item $item) {


        try {
            $rows_deleted = $item->delete();
        }  catch (\Illuminate\Database\QueryException $e) {
            // something went wrong with the transaction, rollback
        } catch (\Exception $e) {
            // something went wrong elsewhere, handle gracefully
        }
        $result = ($rows_deleted == 1) ? true : false;
        return [
            'success'=> $result
        ];
    }


    public function update(Request $request,Item $item) {
        $input = $request->only(['title']);

        try {
            $item->update($input);
            return [
                'update'=>'update',
                'item'=>$item,
            ];
        }  catch (\Illuminate\Database\QueryException $e) {
            // something went wrong with the transaction, rollback
        } catch (\Exception $e) {
            // something went wrong elsewhere, handle gracefully
        }


    }
}
