<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    //
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
    ];

    public function categories() {
        return $this->belongsToMany(Category::class);
    }

    public function getUpdatedAtHumanAttribute(){
        return $this->updated_at->diffForHumans();
    }


    public function toArray()
    {
        $to_array= parent::toArray();
        $to_array['updated_at_human']= $this->updated_at_human;
        return $to_array;
    }


}
