<?php

use Faker\Generator as Faker;

$factory->define(App\Item::class, function (Faker $faker) {
    return [
        //
        'title' => $faker->sentence,
        'short_desc' =>  $faker->sentence,
        'long_desc' => $faker->sentence,
        'thumbnail_url' => 'https://news.nationalgeographic.com/content/dam/news/2016/06/29/adelie_penguin/01_adelie_penguin.ngsversion.1467248400404.adapt.1900.1.jpg',
        'image_url' => 'https://news.nationalgeographic.com/content/dam/news/2016/06/29/adelie_penguin/01_adelie_penguin.ngsversion.1467248400404.adapt.1900.1.jpg'
    ];
});
