<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Items</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

    <link rel="stylesheet" type="text/css" href="/css/app.css">

</head>
<body>
<div class="flex-center position-ref full-height">


    <div class="content">
        <div class="title m-b-md">
            Items
            <div class="container">
               <div class="row">
                   <div class="col-6">A</div>
                   <div class="col-6">B</div>
               </div>
            </div>
        </div>


    </div>
</div>
</body>
</html>
